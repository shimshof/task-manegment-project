var geocoder = new google.maps.Geocoder();
var bounds = new google.maps.LatLngBounds();
var markersArray = [];
var destinationIcon = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=D|FF0000|000000';
var originIcon = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=O|FFFF00|000000';

var number_of_address = 0;

function calculate() {
    //Function to show a map		  
    var service = new google.maps.DistanceMatrixService();
    geocoder.geocode({
        'address': $('.KtoverIsuf').val()
    }, function(a, b) {
        if (b !== 'OK') {
            alert(b);
            return
        }
        var opts = {
            center: a[0].geometry.location,
            zoom: 13
        };
        map = new google.maps.Map(document.getElementById('map-canvas'), opts);

        //Now we have map, lats add distances.
        //
        service.getDistanceMatrix({
            origins: [a[0].geometry.location],
            destinations: $('.inpAddress').map(function() {
                return this.value
            }),
            travelMode: google.maps.TravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: $('#avoidHighways')[0].checked,
            avoidTolls: $('#avoidTolls')[0].checked
        }, function(response, status) {
            if (status != google.maps.DistanceMatrixStatus.OK) {
                alert('Error was: ' + status);
                return;
            }
            var origins = response.originAddresses;
            var destinations = response.destinationAddresses;
            var outputDiv = document.getElementById('outputDiv');
            outputDiv.innerHTML = '';
            deleteOverlays();

            for (var i = 0; i < origins.length; i++) {
                var results = response.rows[i].elements;
                addMarker(origins[i], false);
                for (var j = 0; j < results.length; j++) {
                    addMarker(destinations[j], true);
                    outputDiv.innerHTML += origins[i] + ' to ' + destinations[j] + ': ' + results[j].distance.text + ' in ' + results[j].duration.text + '<br>';
                }
            }


        });

    })
}
$(document).on('click', '#calculate', calculate)


//Copyied from Google
function addMarker(location, isDestination) {
    var icon;
    if (isDestination) {
        icon = destinationIcon;
    } else {
        icon = originIcon;
    }
    geocoder.geocode({
        'address': location
    }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            bounds.extend(results[0].geometry.location);
            map.fitBounds(bounds);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location,
                icon: icon
            });
            markersArray.push(marker);
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}

function deleteOverlays() {
    for (var i = 0; i < markersArray.length; i++) {
        markersArray[i].setMap(null);
    }
    markersArray = [];
}

