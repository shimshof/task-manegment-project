document.getElementById("siteFunctions").innerHTML ;

var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;
var geocoder;
var waypts = [];

function initialize(UserId) {
    
        geocoder = new google.maps.Geocoder();
        directionsDisplay = new google.maps.DirectionsRenderer();

        var mapOptions = {center: { lat: 32.1139, lng: 34.8042}, zoom: 10 };
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        getSourceAddress("פיכמן 3 תל אביב");

        directionsDisplay.setMap(map);
        
        //add all customer addresses to the map/
        $.getJSON('/getCustHelper/?cust=true&id='+UserId, function (selectedData) {
            $.each(selectedData, function (index, product) {
                waypts.push({
                  location:product.Address,
                  stopover:true});
                  //addMarker(product.Address);
            });
        });
        calcRoute()
}

function calcRoute() {
  //var start = document.getElementById('start').value;
  //var end = document.getElementById('end').value;
  var request = {
      origin: "פיכמן 3 תל אביב",//start,
      destination: "brener 5 tel aviv",//end,
      waypoints: waypts,
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING
  };

  directionsService.route(request, function (response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response);
          var summaryPanel = document.getElementById('directions_panel');
          summaryPanel.innerHTML = "";
          summaryPanel.innerHTML = ' <legend><h4>תכנון מסע <h4></legend></br>';
          // For each route, display summary information.
          var route = response.routes[0];
          for (var i = 0; i < route.legs.length; i++) {
              var routeSegment = i + 1;
              summaryPanel.innerHTML += '<b>מנקודה:</b>';
              summaryPanel.innerHTML += route.legs[i].start_address + '</br>';
              summaryPanel.innerHTML += '<b>לנקודה:</b> ' + route.legs[i].end_address + '<br>';
              summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
          }
      } else alert("no service");
  });
  
}
google.maps.event.addDomListener(window, 'load', initialize);

        

function getSourceAddress(data) {
    var address = data;
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        //var marker = new google.maps.Marker({
          //  animation: google.maps.Animation.DROP,
            //icon:'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=O|FFFF00|000000',
            //map: map,
            //position: results[0].geometry.location
        //});
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
    
  }

function addMarker(address) {
      // To add the marker to the map, use the 'map' property
    var myLatlng=geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
            var marker = new google.maps.Marker({
            map: map,
            animation: google.maps.Animation.DROP,
            position: results[0].geometry.location,
            title: address
        });
        marker.setMap(map);
      } 
      else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
      
  }


function reactToViewOptions(UserId) {
    document.getElementById("betweenDates").innerHTML="<input type=Submit value=view />";
    if ($(":selected").text() == "between") {
        document.getElementById("betweenDates").innerHTML = "from: <input type = date name = fromDate /> to <input type = date name = toDate />" +
        "<input type=Submit value=view />";
    }
    else if (($(":selected").text() == "today"))
        document.getElementById("betweenDates").innerHTML = "<input type=Submit value=view />";
    else if (($(":selected").text() == "cust")) {
        document.getElementById("betweenDates").innerHTML = "<select id=customer name = Cust disabled =true> </select> <input type=Submit value=view />";
        getCustomerList(UserId);
    }
    else if (($(":selected").text() == "vehicle")) {
        document.getElementById("betweenDates").innerHTML = "<select id=vehicle name = Vehicle disabled =true> </select> <input type=Submit value=view />";
        getVehicle();
    }
};

function getCustomerList(UserId)
{
    var getCust = $('#customer');
    $.getJSON('/getCustHelper/?id='+UserId, function (selectedData) {
        getCust.attr('disabled', false);
        getCust.empty();
        getCust.append($('<option></<option>').attr('value', '').text('select cust'));
        $.each(selectedData, function (index, product) {
            getCust.append($('<option></<option>').attr('value', product.custName).text(product.custName));
        });
    }); 
}

function getCustInfo(text) {
    $(":input[name=cust]").attr('value', text.value);
    var getAdd = $(":text[name=address]");
    $.getJSON('/getCustHelper/?cust='+text.value, function (selectedData) {
        $.each(selectedData, function (index, product) {
            $(":text[name=address]").attr('value', product.address);
        });
    });    
}

//login section

$(document).ready(function() {
$('a.login-window').click(function() {
    
            //Getting the variable's value from a link 
    var loginBox = $(this).attr('href');

    //Fade in the Popup
    $(loginBox).fadeIn(300);
    
    //Set the center alignment padding + border see css style
    var popMargTop = ($(loginBox).height() + 24) / 2; 
    var popMargLeft = ($(loginBox).width() + 24) / 2; 
    
    $(loginBox).css({ 
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });
    
    // Add the mask to body
    $('body').append('<div id="mask"></div>');
    $('#mask').fadeIn(300);
    
    return false;
});

// When clicking on the button close or the mask layer the popup closed
$('a.close, #mask').live('click', function() { 
  $('#mask , .login-popup').fadeOut(300 , function() {
    $('#mask').remove();  
}); 
return false;
});
});